<?php

namespace Database\Factories;

use App\Models\Club;
use Illuminate\Database\Eloquent\Factories\Factory;

class GameFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'club_first_id' => Club::doesntHave('firstGame')->doesntHave('secondGame')->first()->id,
            'club_second_id' => Club::doesntHave('secondGame')->doesntHave('firstGame')->first()->id + 1,
            'club_first_goals' => random_int(0,5),
            'club_second_goals' => random_int(0,5),
            'week' => 1,
            'played' => 1,
        ];
    }
}
