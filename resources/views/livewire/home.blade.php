<div wire:poll.keep-alive>
    <div class="container mx-auto sm:px-6 lg:px-8 flex justify-end">
        <div class="mt-8 flex">
            <livewire:start-game/>
        </div>
    </div>
    @foreach($seasons as $season)
        <div class="container mx-auto sm:px-6 lg:px-8 flex">
            <div class="mt-8 flex">
                <h2 class="p-6 text-left text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">{{ $season->name }} Season</h2>
            </div>
        </div>

        @foreach($season->games()->where('played', true)->get()->unique('week') as $week)
            <livewire:weeks wire:key="{{$week->id}}" :season="$season" :week="$week->week"/>
        @endforeach
    @endforeach
    <div class="container mx-auto sm:px-10 lg:px-12 ">
        <div class="mt-8">
            {{ $seasons->links() }}
        </div>
    </div>
</div>
