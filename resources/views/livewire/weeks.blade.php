<div>
    <div class="container mx-auto sm:px-6 lg:px-8">
        <div class="mt-8 flex">
            <div class="flex grid grid-cols-1 md:grid-cols-2 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg m-4 w-2/3">
                <div class="p-6">
                    <h2 class="px-3 py-3 text-left text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">League Table</h2>
                    <table class="min-w-full divide-y divide-gray-200 dark:divide-gray-800 table-auto">
                        <thead class="bg-gray-50 dark:bg-gray-900">
                        <tr>
                            <th scope="col" class="px-3 py-3 text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-left">
                                Teams
                            </th>
                            <th scope="col" class="px-3 py-3 text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">
                                PTS
                            </th>
                            <th scope="col" class="px-3 py-3 text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">
                                P
                            </th>
                            <th scope="col" class="px-3 py-3 text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">
                                W
                            </th>
                            <th scope="col" class="px-3 py-3 text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">
                                D
                            </th>
                            <th scope="col" class="px-3 py-3 text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">
                                L
                            </th>
                            <th scope="col" class="px-3 py-3 text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">
                                GD
                            </th>
                        </tr>
                        </thead>
                        <tbody class="divide-gray-200 dark:divide-gray-900">
                        @foreach($clubs as $club)
                            <tr>
                                <td class="px-3 py-2 whitespace-nowrap text-sm font-medium text-gray-600 dark:text-gray-400 text-left">
                                    {{ $club->name }}
                                </td>
                                <td class="px-3 py-2 whitespace-nowrap text-sm text-gray-600 dark:text-gray-400 text-center">
                                    {{ $club->getPoints($season, $week) }}
                                </td>
                                <td class="px-3 py-2 whitespace-nowrap text-sm text-gray-600 dark:text-gray-400 text-center">
                                    {{ $club->getGames($season, $week)->count() }}
                                </td>
                                <td class="px-3 py-2 whitespace-nowrap text-sm text-gray-600 dark:text-gray-400 text-center">
                                    {{ $club->getWins($season, $week)->count() }}
                                </td>
                                <td class="px-3 py-2 whitespace-nowrap text-sm text-gray-600 dark:text-gray-400 text-center">
                                    {{ $club->getDraws($season, $week)->count() }}
                                </td>
                                <td class="px-3 py-2 whitespace-nowrap text-sm text-gray-600 dark:text-gray-400 text-center">
                                    {{ $club->getlost($season, $week)->count() }}
                                </td>
                                <td class="px-3 py-2 whitespace-nowrap text-sm text-gray-600 dark:text-gray-400 text-center">
                                    {{ $club->getGoalDiff($season, $week) }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-t-0 md:border-l">
                    <h2 class="px-6 py-3 text-left text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">Match Results</h2>
                    <table class="min-w-full divide-y divide-gray-200 dark:divide-gray-800 table-auto">
                        <thead class="bg-gray-50 dark:bg-gray-900">
                        <tr>
                            <th scope="col" colspan="3" class="px-6 py-3 text-left text-xs font-medium text-gray-900 dark:text-white uppercase tracking-wider text-center">
                                <span class="ordinal">{{ $week }}th</span> Week Match Result
                            </th>
                        </tr>
                        </thead>
                        <tbody class="divide-y divide-gray-200 dark:divide-gray-800">
                        @foreach($games as $game)
                            <tr>
                                <td class="px-6 py-2 whitespace-nowrap text-sm font-medium text-gray-600 dark:text-gray-400 text-left">
                                    {{ $game->club_first->name ?? null  }}
                                </td>
                                <td class="px-6 py-2 whitespace-nowrap text-sm text-gray-600 dark:text-gray-400 text-center">
                                    {{ $game->club_first_goals ?? null  }} - {{ $game->club_second_goals ?? null  }}
                                </td>
                                <td class="px-6 py-2 whitespace-nowrap text-sm font-medium text-gray-600 dark:text-gray-400 text-right">
                                    {{ $game->club_second->name ?? null }}
                                </td>
                            </tr>
                        @endforeach

                        <!-- More people... -->
                        </tbody>
                    </table>
                </div>

                <div class="p-6 border-t border-gray-200 dark:border-gray-700 col-span-2">
                    <div class="flex justify-between">
                        <livewire:play-all :season="$season"/>
                        <livewire:play :season="$season"/>
                    </div>
                </div>

            </div>
            <div class="flex grid bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg m-4 w-1/3">
                <div class="p-6">
                    <livewire:prediction :season="$season" :week="$week"/>
                </div>
            </div>
        </div>
    </div>
</div>
