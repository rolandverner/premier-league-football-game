<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Season
 *
 * @property int $id
 * @property int $name
 * @property string|null $started_at
 * @property string|null $ended_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Game[] $games
 * @property-read int|null $games_count
 * @method static \Database\Factories\SeasonFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Season newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Season newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Season query()
 * @method static \Illuminate\Database\Eloquent\Builder|Season whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Season whereEndedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Season whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Season whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Season whereStartedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Season whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Season extends Model
{
    use HasFactory;

    public const DEFAULT_START_MONTH = 'August';
    public const DEFAULT_END_MONTH = 'May';

    protected $fillable = ['name', 'started_at', 'ended_at'];

    public function games(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Game::class);
    }

    public function getWeeks()
    {
        return $this->games->unique('week')->pluck('week')->toArray();
    }

    public function getPlayedWeeks()
    {
        return $this->games()->played()->get()->unique('week')->pluck('week')->toArray();
    }

    public function getLeftWeeks(): array
    {
        return array_values(array_diff($this->getWeeks(), $this->getPlayedWeeks()));
    }

    public function getNextWeek()
    {
        $array = array_diff($this->getWeeks(), $this->getPlayedWeeks());
        return reset($array);
    }

}
