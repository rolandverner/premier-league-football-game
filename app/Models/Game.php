<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Game
 *
 * @property int $id
 * @property int|null $season_id
 * @property int $week
 * @property int|null $club_first_id
 * @property int|null $club_second_id
 * @property int $club_first_goals
 * @property int $club_second_goals
 * @property int $played
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Club|null $club_first
 * @property-read \App\Models\Club|null $club_second
 * @property-read \App\Models\Season|null $season
 * @method static \Database\Factories\GameFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Game newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Game newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Game played($type = true)
 * @method static \Illuminate\Database\Eloquent\Builder|Game query()
 * @method static \Illuminate\Database\Eloquent\Builder|Game seasonId($type)
 * @method static \Illuminate\Database\Eloquent\Builder|Game week($type, $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|Game whereClubFirstGoals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Game whereClubFirstId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Game whereClubSecondGoals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Game whereClubSecondId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Game whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Game whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Game wherePlayed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Game whereSeasonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Game whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Game whereWeek($value)
 * @mixin \Eloquent
 */
class Game extends Model
{
    use HasFactory;

    protected $fillable = [
        'season_id',
        'week',
        'club_first_id',
        'club_second_id',
        'club_first_goals',
        'club_second_goals',
        'played'
    ];

    public function season(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Season::class);
    }

    public function club_first(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Club::class, 'club_first_id');
    }

    public function club_second(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Club::class, 'club_second_id');
    }

    public function scopePlayed($query, $type = true)
    {
        return $query->where('played', $type);
    }

    public function scopeSeasonId($query, $type)
    {
        return $query->where('season_id', $type);
    }

    public function scopeWeek($query, $type, $operator = '=')
    {
        return $query->where('week', $operator, $type);
    }


    public function getWinner()
    {
        return match (true) {
            $this->club_first_goals > $this->club_second_goals => $this->club_first,
            $this->club_first_goals < $this->club_second_goals => $this->club_second,
            default => null,
        };
    }

//    public function clubs(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
//    {
//        return $this->belongsToMany(Club::class, 'club_game')
//            ->withPivot('score');
//    }
}
