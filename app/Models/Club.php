<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Club
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $logo
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Game[] $firstGame
 * @property-read int|null $first_game_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Game[] $secondGame
 * @property-read int|null $second_game_count
 * @method static \Database\Factories\ClubFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Club newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Club newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Club query()
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Club extends Model
{
    use HasFactory;

    public const WIN_POINT = 3;

    protected $fillable = ['name', 'logo'];

    public function firstGame(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Game::class, 'club_first_id');
    }

    public function secondGame(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Game::class, 'club_second_id');
    }

    public function getGames($season = null, $week = null): \Illuminate\Database\Eloquent\Collection
    {
        $first_game = $this->firstGame();
        $second_game = $this->secondGame();
        if (isset($season)) {
            $first_game->seasonId($season->id);
            $second_game->seasonId($season->id);
        }
        if (isset($week)) {
            $first_game->week($week, '<=');
            $second_game->week($week, '<=');
        }
        $first_game = $first_game->get();
        $second_game = $second_game->get();
        return $first_game->merge($second_game);
    }

    public function getPoints($season = null, $week = null): float|int
    {
        return ($this->getWins($season, $week)->count() * self::WIN_POINT) + $this->getDraws($season, $week)->count();
    }

    public function getDraws($season = null, $week = null): \Illuminate\Database\Eloquent\Collection
    {
        return $this->getGames()->filter(
            fn(Game $game) => is_null(
                    $game->getWinner()
                ) && (!$season instanceof Season || $game->season_id === $season->id) && (!$week || $game->week <= $week)
        );
    }

    public function getWins($season = null, $week = null): \Illuminate\Database\Eloquent\Collection
    {
        return $this->getGames()->filter(
            fn(Game $game) => $game->getWinner() instanceof Club && $game->getWinner(
                )->id === $this->id && (!$season instanceof Season || $game->season_id === $season->id) && (!$week || $game->week <= $week)
        );
    }

    public function getLost($season = null, $week = null): \Illuminate\Database\Eloquent\Collection
    {
        return $this->getGames()->filter(
            fn(Game $game) => $game->getWinner() instanceof Club && $game->getWinner(
                )->id != $this->id && (!$season instanceof Season || $game->season_id === $season->id) && (!$week || $game->week <= $week)
        );
    }

    public function getGoalDiff($season = null, $week = null): float|int
    {
        return abs($this->goalsScored($season, $week) - $this->goalsReceived($season, $week));
    }

    public function goalsScored($season = null, $week = null): float|int
    {
        $goals = [
            $this->getGames($season, $week)->map(
                fn(Game $game
                ) => $game->club_first_id === $this->id ? $game->club_first_goals : $game->club_second_goals
            )->toArray()
        ];
        return array_sum($goals[0]);
    }

    public function goalsReceived($season = null, $week = null): float|int
    {
        $goals = [
            $this->getGames($season, $week)->map(
                fn(Game $game
                ) => $game->club_first_id === $this->id ? $game->club_second_goals : $game->club_first_goals
            )->toArray()
        ];
        return array_sum($goals[0]);
    }

//

//    public function clubs(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
//    {
//        return $this->belongsToMany(Game::class, 'club_game')
//            ->withPivot('score');
//    }
}
