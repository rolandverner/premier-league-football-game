<?php

namespace App\Http\Livewire;

use App\Services\ClubService;
use Livewire\Component;

class Games extends Component
{
    public function render()
    {
        $clubs = ClubService::getClubs();
        return view('livewire.games',[
            'clubs' => $clubs,
        ]);
    }
}
