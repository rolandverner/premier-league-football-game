<?php

namespace App\Http\Livewire;

use App\Models\Season;
use App\Services\GameService;
use Livewire\Component;

class PlayAll extends Component
{
    public $season;

    public function mount(Season $season){
        $this->season = $season;
    }
    public function playAllGames(){
        GameService::playAll($this->season);
    }

    public function render()
    {
        return view('livewire.play-all');
    }
}
