<?php

namespace App\Http\Livewire;

use App\Services\GameService;
use Livewire\Component;

class Play extends Component
{
    public $season;

    public function playOneGame(){
        GameService::play($this->season, true);
    }

    public function render()
    {
        return view('livewire.play');
    }
}
