<?php

namespace App\Http\Livewire;

use App\Services\SeasonService;
use Livewire\Component;
use Livewire\WithPagination;

class Home extends Component
{
    use WithPagination;

    protected $paginationTheme = 'tailwind';

    protected $listeners = [
        'reRenderSeason'
    ];

    public function reRenderSeason()
    {
        $this->render();
    }

    public function render()
    {
        $seasons = SeasonService::allSeasons(true);
        return view('livewire.home', [
            'seasons' => $seasons,
        ]);
    }
}
