<?php

namespace App\Http\Livewire;

use App\Models\Game;
use App\Models\Season;
use App\Services\ClubService;
use Livewire\Component;

class Weeks extends Component
{
    public $season;
    public $week;

    protected $listeners = [
        'reRenderWeeks'
    ];

    public function mount(Season $season, $week){
        $this->season = $season;
        $this->week = $week;
    }

    public function reRenderWeeks()
    {
        $this->render();
    }

    public function render()
    {
        $weaks = $this->season->games()->where('played', true)->where('week', $this->week)->get();
        $clubs = ClubService::getClubs();
        return view('livewire.weeks',[
            'games' => $weaks,
            'clubs' => $clubs,
        ]);
    }
}
