<?php

namespace App\Http\Livewire;

use App\Services\GameService;
use Livewire\Component;

class Prediction extends Component
{
    public $week;
    public $season;

    public function render()
    {
        $predictions = GameService::getPrediction($this->season, $this->week);
        return view('livewire.prediction',[
            'predictions' => $predictions,
        ]);
    }
}
