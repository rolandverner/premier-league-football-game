<?php

namespace App\Http\Livewire;

use App\Services\GameService;
use Livewire\Component;

class StartGame extends Component
{

    public function startGame(){
        GameService::genGame();
        $this->emit('reRenderSeason');
    }

    public function render()
    {
        return view('livewire.start-game');
    }
}
