<?php

namespace App\Services;


use App\Models\Club;

class ClubService
{
    public static function getClubs($paginate = false): \Illuminate\Database\Eloquent\Collection|\Illuminate\Contracts\Pagination\LengthAwarePaginator|array
    {
        $clubs = Club::query();
        if ($paginate) {
            return $clubs->paginate();
        }
        return $clubs->get();
    }

    public static function getClubsIds(): array
    {
        return Club::pluck('id')->toArray();
    }

    public static function getClubsCount(): int
    {
        return Club::count();
    }
}
