<?php

namespace App\Services;


use App\Models\Club;
use App\Models\Game;
use App\Models\Season;

class GameService
{
    public const WEEK = [
        1 => [[1, 4], [2, 3]],
        2 => [[2, 3], [4, 1]],
        3 => [[3, 2], [1, 4]],
        4 => [[4, 1], [3, 2]]
    ];

    public const GOALS_LIMIT_FROM = 0;
    public const GOALS_LIMIT_TO = 3;

    public static function play($season, $weak)
    {
        self::setGoal($season, $weak);
    }

    public static function playAll($season)
    {
        self::setGoal($season);
    }

    public static function setGoal($season, $weak = false)
    {
        try {
            $games = Game::query();
            $games->where('season_id', $season->id);
            $games->where('played', false);
            if ($weak) {
                if ($season->getNextWeek()) {
                    $games->where('week', '=', $season->getNextWeek());
                }
            }
            $games = $games->get();
            foreach ($games as $game) {
                $game->played = true;
                $game->club_first_goals = self::genGoal();
                $game->club_second_goals = self::genGoal();
                $game->save();
            }
        } catch (\Exception $exception) {
            \Log::error("Can't add goals to game");
        }
    }

    public static function genGame()
    {
        $season = SeasonService::genSeason();

        foreach (self::WEEK as $week => $club) {
            foreach ($club as $set) {
                self::playWithEachOther($season, $week, $set);
            }
        }

        self::setGoal($season, 1);
    }

    public static function playWithEachOther(Season $season, $week, $clubs)
    {
        self::addGame($season, $week, $clubs[0], $clubs[1]);
    }

    public static function addGame(Season $season, $week, $firstTeam, $secondTeam): \Illuminate\Database\Eloquent\Model
    {
        return $season->games()->create([
            'club_first_id' => $firstTeam,
            'club_second_id' => $secondTeam,
            'week' => $week
        ]);
    }

    public static function genGoal(): int
    {
        return random_int(self::GOALS_LIMIT_FROM, self::GOALS_LIMIT_TO);
    }

    public static function getPrediction(Season $season, $week): array
    {
        $clubs = ClubService::getClubs()->map(fn(Club $club) => [
            'id' => $club->id,
            'name' => $club->name,
            'games' => $club->getGames($season, $week)->count(),
            'points' => $club->getPoints($season, $week),
            'draws' => $club->getDraws($season, $week)->count(),
            'wins' => $club->getWins($season, $week)->count(),
            'lost' => $club->getLost($season, $week)->count(),
            'goals' => [
                'scored' => $club->goalsScored($season, $week),
                'received' => $club->goalsReceived($season, $week)
            ]
        ])->toArray();

        //В среднем сколько клуб имеет очков
        $average_points = [];
        foreach ($clubs as $club) {
            $average_points[$club['id']] = $club['games'] ? $club['points'] : 0;
        }

        //В среднем сколько матчей выигрывали клубы
        $average_wins = [];
        foreach ($clubs as $club) {
            $average_wins[$club['id']] = $club['games'] ? $club['wins'] / $club['games'] : 0;
        }

        //В среднем сколько матчей проигрывали
        $average_defeats = [];
        foreach ($clubs as $club) {
            $average_defeats[$club['id']] = $club['games'] ? $club['lost'] / $club['games'] : 0;
        }

        //В среднем сколько матчей играли в ничью
        $average_draws = [];
        foreach ($clubs as $club) {
            $average_draws[$club['id']] = $club['games'] ? $club['draws'] / $club['games'] : 0;
        }

        //Получаем рейтинг нападения. Получить средний показатель забитых голов
        $average_scores = [];
        foreach ($clubs as $club) {
            $average_scores[$club['id']] = $club['games'] ? $club['goals']['scored'] / $club['games'] : 0;
        }

        //Получаем рейтинг защиты. Получить средний показатель пропущенных голов
        $average_received = [];
        foreach ($clubs as $club) {
            $average_received[$club['id']] = $club['games'] ? $club['goals']['received'] / $club['games'] : 0;
        }

        //Вероятность что будут забыты
        $scores = [];
        foreach ($clubs as $club) {
            foreach ($average_scores as $sk => $scored) {
                foreach ($average_received as $rk => $received) {
                    if ($club['id'] != $sk && $club['id'] != $rk && $sk != $rk) {
                        $scores[$club['id']][$rk] = ($scored + $received) / 2;
                    }
                }
            }
        }

        /**
         * Теперь можно округлять учитывая коэффициенты выигрышей,
         * проигрышей и ничейных результатов.
         */

        $scores_win = [];

        foreach ($clubs as $club) {
            $filtered = $average_defeats;
            unset($filtered[$club['id']]);
            $scores_win[$club['id']] = ($average_points[$club['id']] + $average_wins[$club['id']] + $average_draws[$club['id']]) + array_sum(
                    $filtered
                ) / 3;
        }


        $predictions = [];
        foreach ($clubs as $club) {
            foreach ($scores as $sk => $score) {
                foreach ($scores_win as $skw => $score_win) {
                    if ($sk == $skw && $skw == $club['id'] && $sk == $club['id']) {
                        $predictions[$club['name']] = round(array_sum($score) + ($score_win * 4));
                    }
                }
            }
        }
        arsort($predictions);

        $total = array_sum($predictions);
        return array_map(fn(string $value) => number_format(($value / $total) * 100, 2), $predictions);
    }
}
