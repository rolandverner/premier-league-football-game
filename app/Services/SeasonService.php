<?php

namespace App\Services;


use App\Models\Season;

class SeasonService
{
    public static function genSeason(): \Illuminate\Database\Eloquent\Model|Season
    {
        $last = self::lastSeason();
        $season = Season::create();
        if ($last) {
            $season->increment('name', $last->name);
        }
        return $season;
    }

    public static function lastSeason(): \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|Season|null
    {
        return Season::latest()->first();
    }

    public static function allSeasons($paginate = false): \Illuminate\Database\Eloquent\Collection|\Illuminate\Contracts\Pagination\LengthAwarePaginator|array
    {
        $season = Season::query();
        $season->orderByDesc('id');
        return !$paginate ? $season->get() : $season->paginate(1);
    }
}
